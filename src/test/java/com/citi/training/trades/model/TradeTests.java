package com.citi.training.trades.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;


public class TradeTests {
	
	private int testId = 1;
	private String testStock = "GOOGL";
	private int testBuy = 1;
	private double testPrice = 1229.84;
	private long testVolume = 50;

	
	   @Test
	    public void test_Trade_defaultConstructorAndSetters() {
	        Trade testTrade = new Trade();

	        testTrade.setId(testId);
	        testTrade.setStock(testStock);
	        testTrade.setBuy(testBuy);
	        testTrade.setPrice(testPrice);
	        testTrade.setVolume(testVolume);

	        assertEquals("Trade id should be equal to testId",
	                     testId, testTrade.getId());
	        assertEquals("Stock name should be equal to testStock",
	        		testStock, testTrade.getStock());
	        assertEquals("Buy should eqaul to testBuy",
	        		testBuy, testTrade.getBuy());
	        assertEquals("Price should eqaul to testPrice",
	        		testPrice, testTrade.getPrice(), 0.0001);
	        assertEquals("Volume should equal to testVolume",
	        		testVolume, testTrade.getVolume());
	    }
	   
	    @Test
	    public void test_Trade_fullConstructor() {
	        Trade testTrade = new Trade(testId, testStock, testBuy, testPrice, testVolume);

	        assertEquals("Trade id should be equal to testId",
	                     testId, testTrade.getId());
	        assertEquals("Stock name should be equal to testStock",
	                     testStock, testTrade.getStock());
	        assertEquals("Buy option should be equal to testBuy",
	                     testBuy, testTrade.getBuy());
	        assertEquals("Price should equal to testPrice",
                    	 testPrice, testTrade.getPrice(), 0.0001);
	        assertEquals("Volume should be equal to testVolume",
                    	 testVolume, testTrade.getVolume());
	    }
	    
	    @Test
	    public void test_Trade_toString() {
	        Trade testTrade = new Trade(testId, testStock, testBuy, testPrice, testVolume);

	        assertTrue("toString should contain testId",
	                   testTrade.toString().contains(Integer.toString(testId)));
	        assertTrue("toString should contain testStock",
	        		testTrade.toString().contains(testStock));
	        assertTrue("toString should contain testBuy",
	        		testTrade.toString().contains(Integer.toString(testBuy)));
	        assertTrue("toString should contain testPrice",
	        		testTrade.toString().contains(Double.toString(testPrice)));
	        assertTrue("toString should contain testVolume",
	        		testTrade.toString().contains(Long.toString(testVolume)));
	    }
	    
	    @Test(expected=IllegalArgumentException.class)
	    public void test_Trade_invalid_testbuy() { 		    	
	        Trade testTrade = new Trade(testId, testStock, 5, testPrice, testVolume);
	    }
	    
	    @Test(expected=IllegalArgumentException.class)
	    public void test_Trade_invalid_testprice() { 		    	
	        Trade testTrade = new Trade(testId, testStock, testBuy, -100, testVolume);
	    }
	    
	    @Test(expected=IllegalArgumentException.class)
	    public void test_Trade_invalid_testvolume() { 		    	
	        Trade testTrade = new Trade(testId, testStock, testBuy, testPrice, -500);
	    }
	        

}
