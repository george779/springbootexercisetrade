package com.citi.training.trades.rest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.trades.model.Trade;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class TradeControllerIntegrationTests {
	
	private static final Logger LOG = LoggerFactory.getLogger(
            TradeControllerIntegrationTests.class);
	
    @Autowired
    private TestRestTemplate restTemplate;
	
	@Test
    public void getTrade_returnsTrade() {
        String testStock = "MSFT";
        int testBuy = 1;
        double testPrice = 1000;
        long testVolume = 500; 
        
        ///Do A HTTP POST to /stocks
        ResponseEntity<Trade> createTradeResponse = restTemplate.postForEntity("/trades",
                                             new Trade(-1, testStock, testBuy, testPrice, testVolume),
                                             Trade.class);
        
        LOG.info("Create Stock response: " + createTradeResponse.getBody());
        assertEquals(HttpStatus.CREATED, createTradeResponse.getStatusCode());
        assertEquals("created Stock name should equal to testStock",
        		testStock, createTradeResponse.getBody().getStock());
        assertEquals("Buy or sell should equal testBuy",
        		testBuy, createTradeResponse.getBody().getBuy());
        assertEquals("the price should be equal to testPrice",
        		testPrice, createTradeResponse.getBody().getPrice(), 0.0001);
        assertEquals("the volume should be equal to testVolume",
        		testVolume, createTradeResponse.getBody().getVolume());
        
        ///Do a HTTP GET to /stocks/ID
        
        ResponseEntity<Trade> getTradeResponse = restTemplate.getForEntity(
        		"/trades/" + createTradeResponse.getBody().getId(),
        		Trade.class);
        
        LOG.info("Get Trade response: " + getTradeResponse.getBody());
        assertEquals(HttpStatus.OK, getTradeResponse.getStatusCode());
        assertEquals("created Stock name should equal to testStock",
        		testStock, getTradeResponse.getBody().getStock());
        assertEquals("Buy or sell should equal testBuy",
        		testBuy, getTradeResponse.getBody().getBuy());
        assertEquals("the price should be equal to testPrice",
        		testPrice, getTradeResponse.getBody().getPrice(), 0.0001);
        assertEquals("the volume should be equal to testVolume",
        		testVolume, getTradeResponse.getBody().getVolume());
        
        //Delete by ID
        
        ResponseEntity<Trade> deleteTradeResponse = restTemplate.exchange(
        		"/trades/" + createTradeResponse.getBody().getId(),
        		HttpMethod.DELETE, null,
        		new ParameterizedTypeReference<Trade>() {});
        
        LOG.info("DeleteById response: " + deleteTradeResponse.getBody());
        assertEquals(HttpStatus.NO_CONTENT, deleteTradeResponse.getStatusCode());
        
        ResponseEntity<Trade> getFake = restTemplate.getForEntity("/trades/" + 99,
        		Trade.class);
        assertEquals(HttpStatus.NOT_FOUND, getFake.getStatusCode());
                
	}
}
