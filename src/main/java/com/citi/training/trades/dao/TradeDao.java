package com.citi.training.trades.dao;

import org.springframework.data.repository.CrudRepository;

import com.citi.training.trades.model.Trade;

public interface TradeDao extends CrudRepository<Trade, Integer>{

}
