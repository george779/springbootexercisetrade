package com.citi.training.trades.rest;

import org.slf4j.Logger;

/** This is the REST interface for managing trade records.
 * 
 *  @author George Gibbings
 * 
 */
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.trades.dao.TradeDao;
import com.citi.training.trades.model.Trade;


@RestController
@RequestMapping("/trades")
public class TradeController {
	

	    private static final Logger LOG = LoggerFactory.getLogger(TradeController.class);

	    @Autowired
	    private TradeDao tradeDao;

	    /** Mapping using CrudRepository in-built functions, http get to findAll(). 
	     * 
	     * 
	     */
	    @RequestMapping(method=RequestMethod.GET)
	    public Iterable<Trade> findAll() {
	        LOG.info("HTTP GET findAll()");
	        return tradeDao.findAll();
	    }
	    
	    /** http get request.
	     * 
	     */
	    
	    @RequestMapping(value="/{id}", method=RequestMethod.GET)
	    public Trade findById(@PathVariable int id) {
	        LOG.info("HTTP GET findById() id=[" + id + "]");
	        return tradeDao.findById(id).get();
	    }
	    
	    /** http post or save.
	     * 
	     * 
	     */
	    @RequestMapping(method=RequestMethod.POST)
	    public HttpEntity<Trade> save(@RequestBody Trade trade) {
	        LOG.info("HTTP POST save() stock=[" + trade + "]");
	        return new ResponseEntity<Trade>(tradeDao.save(trade), HttpStatus.CREATED);
	    }
	    
	    /**http delete based on id.
	     * 
	     * 
	     */
	    
	    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	    @ResponseStatus(value=HttpStatus.NO_CONTENT)
	    public void deleteById(@PathVariable int id) {
	        LOG.info("HTTP DELETE delete() id=[" + id + "]");
	        tradeDao.deleteById(id);
	    }

}
