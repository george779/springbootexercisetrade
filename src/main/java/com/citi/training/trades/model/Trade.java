package com.citi.training.trades.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Trade {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
    
    @Column(length=10)
	private String stock;
	private int buy;
	private double price;
	private long volume;
	
	/**
	 * Default trade constructor
	 * 
	 * Getter and Setter methods listed.
	 */
	public Trade() {}
	
    public Trade(int id, String stock, int buy, double price, long volume) {
        this.setId(id);
        this.setStock(stock);
        this.setBuy(buy);
        this.setPrice(price);
        this.setVolume(volume);
    }
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getStock() {
		return stock;
	}
	
	public void setStock(String stock) {
		this.stock = stock;
	}
	
	public int getBuy() {
		return buy;
	}
	
	/** Not allowing buy values other than 0 and 1 passing.
	 * 
	 * @param buy
	 */
	
	public void setBuy(int buy) {
		if(buy != 0 && buy != 1) {
			throw new IllegalArgumentException("buy must be either 0 for sell or 1 for buy.");
		}
		else {
			this.buy = buy;
		}
	}
	
	public double getPrice() {
		return price;
	}
	
	/** 
	 * Not allowing negative prices or volumes to be passed.
	 * @param price
	 */
	public void setPrice(double price) {
		if(price < 0) {
			throw new IllegalArgumentException("The price paid must be positive.");
		}
		else {
			this.price = price;
		}
	}
	
	public long getVolume() {
		return volume;
	}
	
	public void setVolume(long volume) {
		if(volume < 0) {
			throw new IllegalArgumentException("Volume can not be negative.");
		}
		else {
			this.volume = volume;
		}
	}

	
	@Override
	public String toString() {
		return "Trade [id=" + id + ", stock=" + stock + ", buy=" + buy + ", price=" + price + ", volume=" + volume
				+ "]";
	}
	

}
